module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'screens-inspector',
      script    : 'src/processes/inspector-sync.js',
      env: {
        "NODE_ENV": "production",
      }
    },
    {
      name      : 'chromecast-inspector',
      script    : 'src/processes/ensure-chromecast.js',
      env: {
        "NODE_ENV": "production",
      }
    }
  ],
};

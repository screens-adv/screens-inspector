var path = require('path');
var setup = require('../setup.js');

module.exports = function () {
    var shared = {};
    var core = {};
    
    var attrs = ['services'];
    attrs.forEach(function (attr) {
        Object.defineProperty(core, attr, {
            set: function () {},
            get: function () {
                return require('./'+attr)(setup, core, shared);
            }
        });
    });
    
    return core;
}
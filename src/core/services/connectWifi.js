var Q = require('q');
var path = require('path');
var fsExtra = require('fs-extra');
var wirelesManager = require('wifi-control');

wirelesManager.init();

module.exports = function (setup, core, shared) {
    return function (ssid, password) {
        var deferred = Q.defer();
        wirelesManager.connectToAP({
            ssid: ssid,
            password: password
        }, function(err, resp) {
            if (err) {
                deferred.reject(err);
                return;
            }
            deferred.resolve(resp);
        });
        return deferred.promise;

    }
}
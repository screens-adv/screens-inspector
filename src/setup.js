// Libs
var path = require('path');
var fsExtra = require('fs-extra');

// Constants
var FILES_PATH = path.join(__dirname, '../files');
var DATA_PATH = path.join(__dirname, '../data');

// Setup files path
fsExtra.ensureDirSync(FILES_PATH);

// Setup data path
fsExtra.ensureDirSync(DATA_PATH);

/**
 * Exports
 */
    // Constants
    exports.constants = {
        FILES_PATH: FILES_PATH,
        DATA_PATH: DATA_PATH,
        INSPECTOR_SYNC_INTERVAL: 60*1000
    }
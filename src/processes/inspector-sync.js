// Imports
var shelljs = require('shelljs');
var express = require('express');
var bodyParser = require('body-parser');
var piWifi = require('pi-wifi');
var internalSetup = require('../setup.js');
var HostInspector = require('host-inspector-module').HostInspector;
// var WirelessManager = require('rpi-wifi-connection');
// var wirelessManager = new WirelessManager();

// Setup Host Inspector
var hostInspector = new HostInspector({
    dataDir: process.env.HOME + '/.screens-data/screens-inspector/data',
    filesDir: process.env.HOME +'/.screens-data/screens-inspector/files',
    screensUrl: process.env.NODE_ENV == 'production' ? 'http://screensads.com' : 'http://localhost:8000',
    filesPort: 3000,
});

// Start inspector
hostInspector.start();

// Setup sync server
var app = express();
app.listen(3969);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/api/test', function (req, res) {
    res.status(200);
    res.send(':)');
    res.end();
});
app.post('/api/sync-device', function (req, res) {
    var deviceCode = req.body.deviceCode;
    hostInspector.setCode(deviceCode);
    res.status(200);
    res.send(':)');
    res.end();
});
app.post('/api/setup-wifi', function (req, res) {
    var data = req.body;
    // wirelessManager.connect({
    //     ssid: data.ssid,
    //     psk: data.password
    // });
    // shelljs.exec('sudo dhclient wlan0');
    // shelljs.exec('sudo wpa_passphrase '+data.ssid+' '+data.password+' >> /etc/wpa_supplicant/wpa_supplicant.conf');
    // shelljs.exec('sudo wpa_cli -i wlan0 reconfigure');
    // wifi.connect({ ssid : data.ssid, password : data.password}, function (err) {
    //     if (err) {
    //         console.log('Error connecting wifi', err);
    //         return;
    //     }
    //     console.log('Connected');
    // });
    piWifi.connect(data.ssid, data.password, function (err) {
        if (err) {
            return console.error(err.message);
        }
        console.log('Successful connection!');
    });
    res.status(200);
    res.send(':)');
    res.end();
});
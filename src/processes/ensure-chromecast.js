var castv2 = require('castv2');
var mdns = require('mdns');
var Client = castv2.Client;
var appId = '3D805EB1';

// Initialize browser
var browser = mdns.createBrowser(mdns.tcp('googlecast'), {
  resolverSequence: [
    mdns.rst.DNSServiceResolve(),
    (
      'DNSServiceGetAddrInfo' in mdns.dns_sd ?
      mdns.rst.DNSServiceGetAddrInfo() :
      mdns.rst.getaddrinfo({
        families: [0]
      })
    ),
    mdns.rst.makeAddressesUnique()
  ]
});

browser.start();

browser.on('serviceUp', function (service) {
  console.log("Device: ", service.addresses[0], ":", service.port);

  // Init client
  var client = new Client();

  // Connect client
  client.connect(service.addresses[0], function () {

    // Setup channels
    var connection = client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.tp.connection', 'JSON');
    heartbeat = client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.tp.heartbeat', 'JSON');
    var receiver = client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.receiver', 'JSON');

    // Send connect instruction
    connection.send({ type: 'CONNECT' });

    // Start pings
    heartbeat.send({ type: 'PING' });
    heartbeat.on('message', function (m) {
      setTimeout(function () {
        heartbeat.send({ type: 'PING' });
      }, 1000);
    });

    // Setup reciever listener
    console.log("Opening Screens App");
    receiver.send({ type: 'LAUNCH', appId: appId, requestId: 1 });

    receiver.on('message', function (data, broadcast) {
      if (
        data.status && data.status.applications &&
        data.status.applications.length && data.status.applications[0].appId !== appId
      ) {
        console.log("Opening Screens App");
        receiver.send({ type: 'LAUNCH', appId: appId, requestId: 1 });
      }
      if (data.reason == 'CAST_INIT_TIMEOUT') {
        receiver.send({ type: 'LAUNCH', appId: appId, requestId: 1 });
      }
    });

  });

});